package com.praxis.jpa.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
@Entity
@Table(name = "user",uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }) })
public class User {
	  @Id
	  @GeneratedValue(strategy = GenerationType.AUTO)
	  private Long id;
	  
	  @Column(name ="username")
	  private String username = null;

	  @Column(name ="firstName")
	  private String firstName = null;

	  @Column(name ="lastName")
	  private String lastName = null;

	  @Column(name ="email")
	  private String email = null;

	  @Column(name ="password")
	  private String password = null;

	  @Column(name ="phone")
	  private String phone = null;

	  @Column(name ="userStatus")
	  private Integer userStatus = null;

	public User(io.swagger.model.User user) {
		this.id = user.getId();
		this.email = user.getEmail();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.password = user.getPassword();
		this.phone = user.getPhone();
		this.username = user.getUsername();
		this.userStatus = user.getUserStatus();
	}
	
	public User() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(Integer userStatus) {
		this.userStatus = userStatus;
	}

	public io.swagger.model.User toSwaggerUser() {
		io.swagger.model.User su = new io.swagger.model.User();
		su.setEmail(email);
		su.setFirstName(firstName);
		su.setId(id);
		su.setLastName(lastName);
		su.setPassword(password);
		su.setPhone(phone);
		su.setUsername(username);
		su.setUserStatus(userStatus);
		return su;
	}
	  

}
