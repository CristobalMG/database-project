package com.praxis.jpa.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.praxis.jpa.models.User;

@Repository

public interface UserManager extends CrudRepository<User,Long>{

	
	List<User> findByUsername(String username);

}
