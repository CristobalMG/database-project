package io.swagger.api;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.praxis.jpa.repository.UserManager;

import io.swagger.model.ModelApiResponse;
import io.swagger.model.User;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Schema;

@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2022-09-12T16:15:14.291Z[GMT]")
@RestController
public class UserApiControllerImpl implements UserApi {

    private static final Logger log = LoggerFactory.getLogger(UserApiControllerImpl.class);

	private final UserManager userRepo;
    private final HttpServletRequest request;

    @org.springframework.beans.factory.annotation.Autowired
    public UserApiControllerImpl(HttpServletRequest request, UserManager userRepo) {
        this.request = request;
        this.userRepo = userRepo;
    }

    public ResponseEntity<User> createUser(@Parameter(in = ParameterIn.DEFAULT, description = "Created user object", schema=@Schema()) @Valid @RequestBody User body) {
       
        if (body != null ) {
            try {
            	com.praxis.jpa.models.User dbUser = new com.praxis.jpa.models.User(body);
            	dbUser = userRepo.save(dbUser);
                return new ResponseEntity<User>(dbUser.toSwaggerUser(), HttpStatus.CREATED);
            }catch (DataIntegrityViolationException e) {
                log.error("Already a user existed for the same username", e);
                return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
            } 
            catch (Exception e) {
                log.error("Couldn't process this request", e);
                return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<ModelApiResponse> createUsersWithListInput(@Parameter(in = ParameterIn.DEFAULT, description = "", schema=@Schema()) @Valid @RequestBody List<User> body) {
        String accept = request.getHeader("Accept");
        if (accept != null && accept.contains("application/json")) {
        	try {
            	com.praxis.jpa.models.User dbUser = new com.praxis.jpa.models.User();
            	ModelApiResponse re = new ModelApiResponse();
            	for(User swaUser : body) {
            	 dbUser = new com.praxis.jpa.models.User(swaUser);
            	 dbUser = userRepo.save(dbUser);
            	}
            	re.setCode(200);
            	re.setMessage("records succesfully created");
            	re.setType("User");
                return new ResponseEntity<ModelApiResponse>(re, HttpStatus.CREATED);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<ModelApiResponse>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<ModelApiResponse>(HttpStatus.NOT_IMPLEMENTED);
    }

    public ResponseEntity<Void> deleteUser(@Parameter(in = ParameterIn.PATH, description = "The name that needs to be deleted", required=true, schema=@Schema()) @PathVariable("username") String username) {
        if (username != null) {
            try {
            	List<com.praxis.jpa.models.User> ul = userRepo.findByUsername(username);
            	if(ul != null && ul.size() > 0) {
            		for(com.praxis.jpa.models.User usr : ul) {
            			userRepo.delete(usr);
            		}
            		return new ResponseEntity<>(HttpStatus.OK);
            	}
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
    

    public ResponseEntity<User> getUserByName(@Parameter(in = ParameterIn.PATH, description = "The name that needs to be fetched. Use user1 for testing. ", required=true, schema=@Schema()) @PathVariable("username") String username) {
        if (username != null) {
            try {
            	List<com.praxis.jpa.models.User> ul = userRepo.findByUsername(username);
            	User u1 = new User();
            	if(ul != null && ul.size() > 0) {
            		u1 = ul.get(0).toSwaggerUser();
            		return new ResponseEntity<User>(u1, HttpStatus.OK);
            	}

            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<User>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<User>(HttpStatus.BAD_REQUEST);
    }

   

	@Override
	public ResponseEntity<User> updateUser(User body) {
		if (body != null ) {
            try {
            	com.praxis.jpa.models.User dbUser = new com.praxis.jpa.models.User(body);
            	log.info("Inside the modify : {}",dbUser);
            	dbUser = userRepo.save(dbUser);
                return new ResponseEntity<User>(dbUser.toSwaggerUser() ,HttpStatus.OK);
            } catch (Exception e) {
                log.error("Couldn't serialize response for content type application/json", e);
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}

}
